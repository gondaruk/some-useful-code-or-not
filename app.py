# from maxfw.core import MAXApp
# from api import ModelMetadataAPI, ModelPredictAPI, ModelLabelsAPI
# from config import API_TITLE, API_DESC, API_VERSION
#
# max_app = MAXApp(API_TITLE, API_DESC, API_VERSION)
# max_app.add_api(ModelMetadataAPI, '/metadata')
# max_app.add_api(ModelLabelsAPI, '/labels')
# max_app.add_api(ModelPredictAPI, '/predict')
# max_app.run()


from pprint import pprint

from flask import Flask
from flask_sockets import Sockets

from api.predict import model_wrapper
from core.model import read_image

app = Flask(__name__)
app.debug = True
sockets = Sockets(app)


@sockets.route('/predict')
def echo_socket(ws):
    while True:
        message = ws.receive()
        print('-' * 50)
        tablet_id = message[0]
        ts = message[1:9]
        image = message[9:]

        # with open('/hostdir/fr_current_image.jpeg', 'wb+') as f:
        #     f.write(image)
        # pprint({
        #     'id': tablet_id,
        #     'ts': ts,
        #     'image_start': image.hex()[0:4],
        #     'image_end': image.hex()[-4:]
        # })

        result = {'status': 'error'}
        img = read_image(image)
        preds = model_wrapper.predict(img)

        label_preds = []
        for res in preds:
            face_emo = {}
            emotion_predictions = []
            # Modify this code if the schema is changed
            for p in res:
                if type(p) is tuple:
                    emo = {'label_id': p[0], 'label': p[1], 'probability': p[2]}
                    emotion_predictions.append(emo)
                if type(p) is list:
                    face_emo['detection_box'] = p
            face_emo['emotion_predictions'] = emotion_predictions
            label_preds.append(face_emo)
        result['predictions'] = label_preds
        result['status'] = 'ok'

        pprint(result)

        # send keep-alive
        ws.send(0x00)

@app.route('/')
def hello():
    return 'Hello World!'
