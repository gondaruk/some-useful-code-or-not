# Sample Assets

## Images

All test images are from [Pexels](https://www.pexels.com) and licensed under a [CC0 License](https://creativecommons.org/publicdomain/zero/1.0/) or [Pexels License](https://www.pexels.com/photo-license/). Some images are cropped to the face portion of the image.

* [angry.jpg](https://www.pexels.com/photo/portrait-angry-closeup-black-and-white-18175/)
* [happy-baby.jpeg](https://www.pexels.com/photo/baby-lying-on-fur-1648383/)
* [neutral.jpeg](https://www.pexels.com/photo/woman-in-red-tank-top-638700/)
* [sad-baby.jpeg](https://www.pexels.com/photo/baby-child-close-up-crying-47090/)
* [surprised.jpeg](https://www.pexels.com/photo/man-in-santa-claus-costume-716658/)
* [group.jpeg](https://www.pexels.com/photo/group-of-people-in-a-meeting-1080865/)
